Mini-projet : doc
=================

Tout ce qu'il faut savoir sur Linux et Git
------------------------------------------

Le but de ce projet est de constituer des documents Markdown ou Latex
qui rassemble les connaissances que vous tirez des cours "Outils
informatiques" (Linux et Système de gestion de version).

Il s'agira de compléter les documents présents dans les sous-dossiers
`markdown` et `latex` de ce dépôt.

**Important** :

-   Coordonnez-vous, répartissez-vous des sections, des rôles, des
    commandes à décrire. Ça vous permettra d'éviter à avoir à résoudre
    trop de conflits.
-   Deux personnes seront choisies pour avoir des droits de *push*.
    Elles seront chacunes responsables d'un des deux documents. Les
    autres devront passer par des *merge requests*, que les responsables
    accepteront ou non.
