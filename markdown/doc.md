Linux
=====

Historique
----------

Aides des commandes
-------------------

Git
===

Historique
----------

Aides des commandes
-------------------

Markdown ?
======

Petite aide :

-   Cf. le code pour voir comment faire une liste à puces.
-   Utiliser `1.` à la place des `-` pour faire une liste numérotée.
-   Un saut de ligne simple est ignoré, un double saut de ligne sépare
    deux paragraphes.
-   Pour formatter le texte :
    -   la syntaxe `` `de code...` `` permet d'écrire des petits bouts
        `de code...` (apostrophe inversée : `AltGr-7`).
    -   Des indentations cohérentes (au moins quatre) permettent de
        mettre plusieurs lignes de code en forme, pour montrer par
        exemple :
            
            $ git log --oneline
            13040e0 (HEAD -> master, origin/master) Ajoute mini-projet documentation
            190c664 Corrige typo exo2
            d66e9ec Ajoute mini-projet simuRPG

    -   `_texte_` met le _texte_ en italique.
    -   `**texte**` met le **texte** en gras.
-   Libre à vous de compléter, éventuellement !
